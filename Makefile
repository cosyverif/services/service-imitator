SHELL := /bin/bash # Use bash syntax

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

clean:          ## Cleanup project files
	mvn clean

deps:           ## install dependencies
	xargs apt-get -y install < RUNTIME-DEPENDENCIES

build:          ## Build project with running all tests
	mvn package

install:        ## Install the project to the local repository
	mvn install

.PHONY: help clean deps build install
