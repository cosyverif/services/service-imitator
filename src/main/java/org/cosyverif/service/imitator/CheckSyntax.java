package org.cosyverif.service.imitator;

import java.io.File;
import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.cosyverif.alligator.service.AnnotatedService.Task;
import org.cosyverif.alligator.service.BinaryService;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.annotation.Launch;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.alligator.service.annotation.Service;
import org.cosyverif.alligator.service.output.LineOutputHandler;
import org.cosyverif.model.Model;

@Service(name = "Check Syntax", help = "Check syntax of the model", version = "3.3", tool = "IMITATOR", index = 4, authors = {
        "Étienne André"
}, keywords = {})
public class CheckSyntax extends BinaryService {
    GrmlParser parser;

    // Model file
    @Parameter(name = "Model", help = "Model of the real-time system to be verified.", direction = Direction.IN, formalism = {
            "http://formalisms.cosyverif.org/parametric-timed-automaton.fml",
            "http://formalisms.cosyverif.org/network-pta.fml"
    })
    public Model model;

    @Parameter(name = "Logs", help = "Imitator logs", direction = Direction.OUT, multiline = true)
    public String logs = "";

    @Launch
    public Task runCheck() throws IOException {
        parser = GrmlParser.create(model);
        final File model = parser.parse();

        // imitator command
        final CommandLine command = new CommandLine("./imitator/imitator");
        command.addArgument(model.getAbsolutePath());

        return task(command).out(new LineOutputHandler(this) {
            @Override
            public void call(String line) {
                logs += line.replaceAll("\u001B\\[[;\\d]*m", "") + '\n';
            }
        });
    }
}
