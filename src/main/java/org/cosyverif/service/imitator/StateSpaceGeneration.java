package org.cosyverif.service.imitator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import org.apache.commons.exec.CommandLine;
import org.cosyverif.alligator.service.AnnotatedService.Task;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.annotation.Example;
import org.cosyverif.alligator.service.annotation.Launch;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.alligator.service.annotation.Service;
import org.cosyverif.alligator.service.output.LineOutputHandler;

@Service(name = "State Space Generation", help = "Generation of the entire parametric state space.", version = "3.3", tool = "IMITATOR", index = 1, authors = {
    "Étienne André"
}, keywords = {})
public final class StateSpaceGeneration extends Imitator {
  // modes for drawing the state space
  enum DrawMode {
    NONE,
    UNDETAILED,
    NORMAL,
    FULL
  };

  // Draw state space
  @Parameter(name = "Draw state space", help = "Draw the state space in a graphical form", direction = Direction.IN)
  public DrawMode drawMode = DrawMode.NORMAL;

  @Parameter(name = "Generate description", help = "Generate the description of all reachable states in a text file", direction = Direction.IN)
  public boolean description = false;

  // Result
  @Parameter(name = "Output", help = "The result of the call", direction = Direction.OUT, contenttype = "plain/text")
  public File outputFile = null;

  @Parameter(name = "Figure", help = "Graphical form of the state space", direction = Direction.OUT, contenttype = "application/pdf")
  public File stateSpaceFigure = null;

  @Parameter(name = "Description", help = "Description of all reachable states ", direction = Direction.OUT, contenttype = "plain/text")
  public File descriptionFile = null;

  @Launch
  public Task launchGeneration() throws IOException {

    parser = GrmlParser.create(model);
    final File model = parser.parse();

    // imitator command
    final CommandLine command = new CommandLine("./imitator/imitator");
    fillOptions(command);

    // additional options
    command.addArguments("-mode statespace");

    // draw state space
    boolean draw = drawMode != DrawMode.NONE;
    if (draw) {
      command.addArguments("-draw-statespace " + drawMode.toString().toLowerCase());
    }

    // save the state-space in a file
    if (description) {
      command.addArguments("-states-description");
    }

    // add model
    command.addArgument(model.getAbsolutePath());

    // output file
    final String outputFilePath = Paths.get(model.getParent(), model.getName()).toString();
    return task(command).out(new LineOutputHandler(this) {

      @Override
      public void call(String line) {
        logs += line.replaceAll("\u001B\\[[;\\d]*m", "") + '\n';

        // return output file
        File f = new File(outputFilePath + ".res");
        File fig = new File(outputFilePath + "-statespace.pdf");
        File desc = new File(outputFilePath + "-statespace.states");

        if (description && desc.exists()) {
          descriptionFile = desc;
        }

        if (draw && fig.exists()) {
          stateSpaceFigure = fig;
        }

        if (f.exists()) {
          outputFile = f;
        }
      }
    });
  }

  @Example(name = "Example of state space generation", help = "Example of statespace generation")
  public StateSpaceGeneration example() {
    StateSpaceGeneration service = new StateSpaceGeneration();
    service.model = loadModelResource("/models/coffee.grml");
    return service;
  }
}
