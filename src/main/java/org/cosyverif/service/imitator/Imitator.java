package org.cosyverif.service.imitator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.commons.exec.CommandLine;
import org.cosyverif.alligator.service.BinaryService;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.alligator.service.annotation.Refresh;
import org.cosyverif.model.Model;

public abstract class Imitator extends BinaryService {
    GrmlParser parser;

    public static int UNKNOWNINT = -1;

    enum VerboseMode {
        MUTE,
        WARNINGS,
        STANDARD,
        EXPERIMENTS,
        LOW,
        MEDIUM,
        HIGH,
        TOTAL
    };

    // Model file
    @Parameter(name = "Model", help = "Model of the real-time system to be verified.", direction = Direction.IN, formalism = {
            "http://formalisms.cosyverif.org/parametric-timed-automaton.fml",
            "http://formalisms.cosyverif.org/network-pta.fml"
    })
    public Model model;

    // Option: -depth-limit
    @Parameter(name = "Depth limit", help = "Maximum depth in the exploration of the state space.", direction = Direction.IN)
    public int depthLimit = UNKNOWNINT;

    // Option: -states-limit
    @Parameter(name = "States limit", help = "Maximum number of states to explore (this is only a tentative number for IMITATOR).", direction = Direction.IN)
    public int statesLimit = UNKNOWNINT;

    // Option: -statistics
    @Parameter(name = "With statistics", help = "Prints statistics (e.g., number of calls to PPL)", direction = Direction.IN)
    public boolean statistics = false;

    // Option: -time-limit
    @Parameter(name = "Time limit", help = "Maximum number of seconds (this is only a tentative number for IMITATOR).", direction = Direction.IN)
    public int timeLimit = UNKNOWNINT;

    // Option: -timed
    @Parameter(name = "Timed mode", help = "Add time information for each line output.", direction = Direction.IN)
    public boolean timed = false;

    // Option: -no-var-autoremove
    @Parameter(name = "Remove unused variables", help = "Prevent the automatic removal of variables (discrete, clocks, parameters) declared in the header but never used in the IPTAs", direction = Direction.IN)
    public boolean varAutoremove = true;

    // Option: -verbose
    @Parameter(name = "Verbose mode", help = "Quantity of information given.", direction = Direction.IN)
    public VerboseMode verboseMode = VerboseMode.STANDARD;

    @Parameter(name = "Logs", help = "Imitator logs", direction = Direction.OUT, contenttype = "plain/text")
    public File logsFile = null;

    // buffer with logs
    public String logs = "";

    public CommandLine fillOptions(CommandLine cl) {

        // Handle verbose mode
        cl.addArguments("-verbose " + verboseMode.toString().toLowerCase());

        // Int options
        if (depthLimit != UNKNOWNINT) {
            cl.addArguments("-depth-limit " + depthLimit);
        }
        if (statesLimit != UNKNOWNINT) {
            cl.addArguments("-states-limit " + statesLimit);
        }
        if (timeLimit != UNKNOWNINT) {
            cl.addArgument("-time-limit " + timeLimit);
        }

        // Booleans
        if (statistics) {
            cl.addArgument("-statistics");
        }
        if (timed) {
            cl.addArgument("-timed");
        }
        if (!varAutoremove) {
            cl.addArgument("-no-var-autoremove");
        }

        return cl;
    }

    /*
     * Methods annotated with @Refresh are run before data is sent to the clients.
     * If you have a small computation to perform fko, 2013-07-17 - un nouveau se
     * only when asked by the client, put it here.
     */
    @Refresh
    public void refresh() {
        BufferedWriter writer;
        try {
            logsFile = Files.createTempFile(null, ".txt").toFile();
            writer = new BufferedWriter(new FileWriter(logsFile.toString()));
            writer.write(logs);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
