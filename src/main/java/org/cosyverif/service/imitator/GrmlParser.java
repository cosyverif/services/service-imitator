package org.cosyverif.service.imitator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cosyverif.Configuration;
import org.cosyverif.model.Arc;
import org.cosyverif.model.Attribute;
import org.cosyverif.model.Model;
import org.cosyverif.model.Node;
import org.cosyverif.model.Ref;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.NONE)
@Accessors(chain = true, fluent = true)
public class GrmlParser {
  /** GRML Model */
  private Model model;

  /**
   * Constructor
   *
   * @param model Grml Model
   */
  public GrmlParser(Model model) {
    this.model = model;
  }

  /**
   * Creates a parser from Grml to Imitator
   *
   * @param model Grml Model
   * @return GrmlParser object
   */
  public static GrmlParser create(Model model) {
    return new GrmlParser(model);
  }

  /**
   * Generates a file handled by imitator
   *
   * @return File object of the generated file
   * @throws IOException
   */
  public File parse() throws IOException {

    /** Create Imitator input file */
    File outputDirectory = Configuration.instance().temporaryDirectory();
    File fileOutput = File.createTempFile("imitator_", "", outputDirectory);
    BufferedWriter writer = new BufferedWriter(new FileWriter(fileOutput));

    /** parse PTA from GrML to Imitator */
    String imitatorCode = parse(model).toImitator();

    /** write content */
    writer.write(imitatorCode);
    writer.close();

    return fileOutput;
  }

  /**
   * This method is used to extract the value of an attribute with the given name
   * from a list of objects.
   *
   * @param attributeList the list of attributes to search for the attribute
   * @param name          the name of the attribute whose value is to be extracted
   * @return the value of the attribute with the given name, otherwise null
   */
  private <E> List<Attribute> findAttributeByName(List<E> attributeList, String name) {
    List<Attribute> found = new ArrayList<Attribute>();
    for (E object : attributeList) {
      if (object instanceof Attribute) {
        Attribute attr = (Attribute) object;
        if (attr.getName().equals(name)) {
          found.add(attr);
        }
      }
    }
    return found;
  }

  /**
   * This method gets the content of an Attribute and trim it.
   * For instance, with <attribute name="name">d1</attribute> as input, the
   * method will return d1.
   *
   * @param attribute Attribute
   * @return string of the value
   */
  private String getTextAttribute(Attribute attribute) {
    return ((String) attribute.getContent().get(0)).trim();
  }

  /**
   * Return all the content of an attribute that is not empty
   *
   * @param attr Attribute
   * @return List of attribute inside the attribute
   */
  private List<Attribute> getContent(Attribute attr) {
    List<Attribute> attrs = new ArrayList<Attribute>();
    for (Object e : attr.getContent()) {
      if (e instanceof Attribute) {
        attrs.add((Attribute) e);
      }
    }
    return attrs;
  }

  /**
   * Extracts the information about the nodes in the model and returns a list of
   * states. Each state contains information about the states's id, type,
   * name, and invariant.
   *
   * @param model GrML model
   * @return a list of states representing the states in the model
   */
  private List<State> parseStates(Model model) {
    List<State> states = new ArrayList<State>();

    for (Node node : model.getNode()) {
      // skip submodel
      List<Object> nodeContent = node.getNodeContent();
      if (nodeContent.size() > 0 && nodeContent.get(0).getClass() == Ref.class) {
        continue;
      }

      // get ID of the state
      State state = new State(node.getId().toString());

      /** parse name */
      List<Attribute> nameAttr = findAttributeByName(node.getNodeContent(), "name");
      if (nameAttr.size() == 1) {
        state.name(getTextAttribute(nameAttr.get(0)));
      }

      /** parse initial state */
      List<Object> typeAttr = findAttributeByName(node.getNodeContent(), "type").get(0).getContent();
      List<Attribute> initialStateAttr = findAttributeByName(typeAttr, "initialState");
      if (initialStateAttr.size() == 1) {
        state.initialState(true);
      }

      /** parse final state */
      List<Attribute> finalStateAttr = findAttributeByName(typeAttr, "finalState");
      if (finalStateAttr.size() == 1) {
        state.finalState(true);
      }

      /** parse invariant */
      List<Object> invariantAttr = findAttributeByName(node.getNodeContent(), "invariant").get(0).getContent();
      List<Attribute> booleanAttr = findAttributeByName(invariantAttr, "boolExpr");
      if (booleanAttr.size() == 1) {
        Attribute booleanExpr = booleanAttr.get(0);
        if (booleanExpr.getContent().size() == 1) {
          state.invariant(getTextAttribute(booleanExpr));
        }
      }

      states.add(state);
    }

    return states;

  }

  /**
   * Extracts the information about the transitions in the model and returns a
   * list of transitions. Each transition contains information about the
   * transition's id, source, target, label, guard, and updates.
   *
   * @param model  GrML model
   * @param states list of state of the model
   * @return a list of transitions in the model
   */
  private List<Transition> parseTransitions(Model model, List<State> states) {
    List<Transition> transitions = new ArrayList<Transition>();

    for (Arc arc : model.getArc()) {
      /** Get the ID of the transition */
      String id = arc.getId().toString();

      State source = states.stream().filter(s -> s.id().equals(arc.getSource().toString())).findAny().orElse(null);
      State target = states.stream().filter(s -> s.id().equals(arc.getTarget().toString())).findAny().orElse(null);

      Transition transition = new Transition(id, source.name(), target.name());
      source.addOutgoingTransition(transition);
      target.addIncomingTransition(transition);

      // parse label
      Attribute labelAttr = findAttributeByName(arc.getArcContent(), "label").get(0);
      if (labelAttr.getContent().size() == 1) {
        transition.label(getTextAttribute(labelAttr));
      }

      // parse updates
      List<Object> updates = findAttributeByName(arc.getArcContent(), "updates").get(0).getContent();
      for (Attribute update : findAttributeByName(updates, "update")) {
        List<Attribute> content = getContent(update);
        // an update has an attribute name and an attribute expr
        if (content.size() == 2) {
          Attribute name = findAttributeByName(content, "name").get(0);
          Attribute expr = findAttributeByName(content, "expr").get(0);
          if (name.getContent().size() == 1 && expr.getContent().size() == 1) {
            Update u = new Update(getTextAttribute(name), getTextAttribute(expr));
            transition.addUpdate(u);
          }
        }
      }

      // parse guard
      List<Object> invariantAttr = findAttributeByName(arc.getArcContent(), "guard").get(0).getContent();
      List<Attribute> booleanAttr = findAttributeByName(invariantAttr, "boolExpr");
      if (booleanAttr.size() == 1) {
        Attribute booleanExpr = booleanAttr.get(0);
        if (booleanExpr.getContent().size() == 1) {
          transition.guard(getTextAttribute(booleanExpr));
        }
      }

      transitions.add(transition);
    }

    return transitions;
  }

  /**
   * Extracts the name of the model from the model object.
   *
   * @param model GrML model
   * @return the name of the model
   */
  private String parseModelName(Model model) {
    // Iterate through the attributes of the model
    for (Attribute attribute : model.getAttribute()) {
      // If the attribute is the name attribute, return its value
      if (attribute.getName().equals("name") && attribute.getContent().size() == 1) {
        return getTextAttribute(attribute);
      }
    }
    // If no name attribute was found, return the default value "machine"
    return "machine";
  }

  /**
   * Extracts the information about the automaton in the model and return an
   * automaton.
   *
   * @param model GrML model
   * @return an automaton
   */
  private Automaton parseAutomaton(Model model) {
    /** parsing the name of the automaton */
    Automaton automaton = new Automaton(parseModelName(model));

    /** parsing states */
    automaton.states(parseStates(model));

    /** parsing transitions */
    automaton.transitions(parseTransitions(model, automaton.states()));

    return automaton;
  }

  /**
   * Parse a GrML file into an Imitator model
   *
   * @param model GrML model
   * @return an imitator model
   */
  private ParametricTimedAutomaton parse(Model model) {
    /** parsing the name of the formalism */
    ParametricTimedAutomaton imitator = new ParametricTimedAutomaton(parseModelName(model));

    List<Model> models = Arrays.asList(model);
    if (model.getFormalismUrl().contains("network")) {
      // set imitator as a network of PTAs
      imitator.isNetwork(true);

      // parse sync labels
      List<Object> syncAttr = findAttributeByName(model.getAttribute(), "syncLabels").get(0).getContent();
      for (Object sync : findAttributeByName(syncAttr, "syncLabel")) {
        List<Attribute> labels = findAttributeByName(((Attribute) sync).getContent(), "label");
        if (labels.size() == 1) {
          if (labels.get(0).getContent().size() > 0) {
            String label = getTextAttribute(labels.get(0));
            imitator.addSyncLabel(label);
          }
        }
      }
      models = model.getModel();
    }

    for (Model m : models) {
      imitator.addAutomaton(parseAutomaton(m));

      List<Attribute> modelAttributes = m.getAttribute();

      // Parse clocks
      List<Object> clocksAttr = findAttributeByName(modelAttributes, "clocks").get(0).getContent();
      for (Object clk : findAttributeByName(clocksAttr, "clock")) {
        List<Attribute> names = findAttributeByName(((Attribute) clk).getContent(), "name");
        if (names.size() == 1) {
          if (names.get(0).getContent().size() > 0) {
            String name = getTextAttribute(names.get(0));
            imitator.addClock(name);
          }
        }
      }

      // Parse discrete variables
      List<Object> variablesAttr = findAttributeByName(modelAttributes, "variables").get(0).getContent();
      List<Object> discreteVariablesAttr = findAttributeByName(variablesAttr, "discretes").get(0).getContent();
      for (Object var : findAttributeByName(discreteVariablesAttr, "discrete")) {
        List<Attribute> names = findAttributeByName(((Attribute) var).getContent(), "name");
        if (names.size() == 1) {
          if (names.get(0).getContent().size() > 0) {
            String name = getTextAttribute(names.get(0));
            imitator.addDiscreteVariable(name);
          }
        }
      }

      // get constants
      List<Object> constantsAttr = findAttributeByName(modelAttributes, "constants").get(0).getContent();
      for (Object c : findAttributeByName(constantsAttr, "const")) {
        List<Attribute> names = findAttributeByName(((Attribute) c).getContent(), "name");
        List<Attribute> expressions = findAttributeByName(((Attribute) c).getContent(), "expr");
        if (names.size() == 1) {
          if (names.get(0).getContent().size() > 0) {
            String name = getTextAttribute(names.get(0));
            if (expressions.size() == 1) {
              if (expressions.get(0).getContent().size() > 0) {
                String value = getTextAttribute(expressions.get(0));
                imitator.addConstant(name, value);
              }
            }
          }
        }
      }

      // get parameters
      List<Object> parametersAttr = findAttributeByName(modelAttributes, "parameters").get(0).getContent();
      for (Object p : findAttributeByName(parametersAttr, "parameter")) {
        List<Attribute> names = findAttributeByName(((Attribute) p).getContent(), "name");
        if (names.size() == 1) {
          if (names.get(0).getContent().size() > 0) {
            String name = getTextAttribute(names.get(0));
            imitator.addParameter(name);
          }
        }
      }

      // get initial constraint
      List<Object> initialConstraintAttr = findAttributeByName(modelAttributes, "initialConstraint").get(0)
          .getContent();
      for (Attribute expr : findAttributeByName(initialConstraintAttr, "boolExpr")) {
        if (expr.getContent().size() == 1) {
          imitator.addInitialConstraint(getTextAttribute(expr));
        }
      }
    }

    return imitator;
  }
}
