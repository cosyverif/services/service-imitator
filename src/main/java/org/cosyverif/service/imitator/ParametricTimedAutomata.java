package org.cosyverif.service.imitator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@NoArgsConstructor
@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class ClockDeclaration {
  /** list of clocks */
  private Set<String> clocks = new HashSet<String>();

  /**
   * Return the Imitator code of the clocks declaration
   *
   * @return Imitator code
   */
  public String toImitator() {
    StringBuilder sb = new StringBuilder();
    if (!clocks.isEmpty()) {
      sb.append("(* Clocks *)").append("\n");
      sb.append("\t");

      for (String clock : clocks) {
        sb.append(clock).append(", ");
      }

      // remove the last comma and space
      sb.delete(sb.length() - 2, sb.length());
      sb.append("\n\t\t: clock;\n\n");
    }
    return sb.toString();
  }
}

@NoArgsConstructor
@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class DiscreteDeclaration {
  /** List of discrete variables */
  private Set<String> discretes = new HashSet<String>();

  /**
   * Return the Imitator code of the discrete variables declaration
   *
   * @return Imitator code
   */
  public String toImitator() {
    StringBuilder sb = new StringBuilder();
    if (!discretes.isEmpty()) {
      sb.append("(* Discrete variables *)").append("\n");
      sb.append("\t");
      for (String discrete : discretes) {
        sb.append(discrete).append(", ");
      }
      // remove the last comma and space
      sb.delete(sb.length() - 2, sb.length());
      sb.append("\n\t\t: discrete;\n\n");
    }
    return sb.toString();
  }
}

@NoArgsConstructor
@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class ParameterDeclaration {
  /** list of parameters */
  private Set<String> parameters = new HashSet<String>();

  /**
   * Return the Imitator code of the parameters declaration
   *
   * @return String
   */
  public String toImitator() {
    StringBuilder sb = new StringBuilder();
    if (!parameters.isEmpty()) {
      sb.append("(* Parameters *)").append("\n");
      sb.append("\t");
      for (String parameter : parameters) {
        sb.append(parameter).append(", ");
      }
      // remove the last comma and space
      sb.delete(sb.length() - 2, sb.length());
      sb.append("\n\t\t: parameter;\n\n");
    }
    return sb.toString();
  }
}

@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class Constant {
  /** name of the constant */
  private String name;
  /** constant value */
  private String value;

  @Override
  public boolean equals(Object o) {
    if (o != null && getClass() == o.getClass()) {
      Constant other = (Constant) o;
      return name.equals(other.name()) && value.equals(other.value());
    }
    return false;
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  /**
   * Return the Imitator code of a constant
   *
   * @return Imitator code
   */
  public String toImitator() {
    StringBuilder sb = new StringBuilder();
    sb.append(name);
    if (value != null) {
      sb.append(" = " + value);
    }
    return sb.toString();
  }
}

@NoArgsConstructor
@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class ConstDeclaration {
  /** List of constants */
  private Set<Constant> constants = new HashSet<Constant>();

  /**
   * Return the imitator code of the constants declaration
   *
   * @return Imitator code
   */
  public String toImitator() {
    StringBuilder sb = new StringBuilder();
    if (!constants.isEmpty()) {
      sb.append("(* Constants *)").append("\n");
      sb.append("\t");
      for (Constant constant : constants) {
        sb.append(constant.toImitator()).append(", ");
      }
      // remove the last comma and space
      sb.delete(sb.length() - 2, sb.length());
      sb.append("\n\t\t: constant;\n\n");
    }
    return sb.toString();
  }
}

@NoArgsConstructor
@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class Update {
  /** name of the variable updated */
  private String name;
  /** expression of the update */
  private String expr;

  /**
   * Return the imitator code of the updates
   *
   * @return Imitator code
   */
  public String toImitator() {
    StringBuilder sb = new StringBuilder();
    sb.append(name).append(" := ").append(expr);
    return sb.toString();
  }
}

@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class Transition {
  /** identifier of the transition */
  private String id;
  /** state source of the transition */
  private String source;
  /** state target of the transition */
  private String target;
  /** list of updates of the transition */
  private List<Update> updates = new ArrayList<Update>();
  /** guard of the transition */
  private String guard = "True";
  /** label of the transition */
  private String label = null;

  /**
   * Constructor
   *
   * @param id     identifier of the transition
   * @param source identifier of the source state
   * @param target identifier of the target state
   */
  public Transition(String id, String source, String target) {
    this.id = id;
    this.source = source;
    this.target = target;
  }

  /**
   * Add a variable update to the transition
   *
   * @param update new update to be added
   */
  public void addUpdate(Update update) {
    this.updates.add(update);
  }

  /**
   * Return the Imitator code of the transition
   *
   * @return Imitator code
   */
  public String toImitator() {
    StringBuilder sb = new StringBuilder();
    sb.append("\twhen ").append(guard);

    // add synclabels
    if (label != null) {
      sb.append(" sync ").append(label);
    }

    sb.append(" do {");
    for (Update update : updates) {
      sb.append(update.toImitator()).append(", ");
    }
    // remove the last comma and space
    if (!updates.isEmpty()) {
      // Si la liste n'est pas vide
      sb.delete(sb.length() - 2, sb.length());
    }
    sb.append("} goto ").append(target).append(";\n");
    return sb.toString();
  }
}

@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class State {
  /** Identifier of the state */
  private String id;

  /** state label */
  private String name;

  /** state invariant */
  private String invariant = "True";

  /** outgoing transitions */
  private List<Transition> outgoingTransitions = new ArrayList<Transition>();

  /** incoming tranistions */
  private List<Transition> incomingTransitions = new ArrayList<Transition>();

  /** flag indicating if the state is an initial state */
  private Boolean initialState = false;

  /** flag indicating if the state is a final state */
  private Boolean finalState = false;

  /**
   * Constructor of the class
   *
   * @param id identifier of the state
   */
  public State(String id) {
    this.id = id;
  }

  /**
   * Add outgoing transition
   *
   * @param transition transition to be added
   */
  public void addOutgoingTransition(Transition transition) {
    outgoingTransitions.add(transition);
  }

  /**
   * Add incomming transition
   *
   * @param transition transition to be added
   */
  public void addIncomingTransition(Transition transition) {
    incomingTransitions.add(transition);
  }

  /**
   * Return the imitator code of the state
   *
   * @return imitator code
   */
  public String toImitator() {
    StringBuilder sb = new StringBuilder();
    sb.append("loc ").append(name).append(": invariant ").append(invariant).append("\n");
    for (Transition t : outgoingTransitions) {
      sb.append(t.toImitator());
    }
    return sb.toString();
  }
}

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class Automaton {
  private String name;
  private List<State> states = new ArrayList<State>();
  private List<Transition> transitions = new ArrayList<Transition>();

  /**
   * Constructir of the class
   *
   * @param name name of the automaton
   */
  public Automaton(String name) {
    this.name = name;
  }

  /**
   * Return the synclabels of the automaton
   *
   * @return List of labels
   */
  public List<String> getSyncLabels() {
    return transitions.stream().map(t -> t.label()).filter(t -> t != null).distinct()
        .collect(Collectors.toList());
  }

  /**
   * Return the imitator code of the automaton
   *
   * @param networkSyncLabels Set of the labels to be syncronized
   * @return Imitator code
   */
  public String toImitator(Set<String> networkSyncLabels) {
    StringBuilder sb = new StringBuilder();

    // parse name
    sb.append("(************************************************************)\n");
    sb.append(" automaton " + name + "\n");
    sb.append("(************************************************************)\n");

    // Write the labels of the arcs as synclabs
    List<String> synclabs = getSyncLabels().stream().distinct().filter(networkSyncLabels::contains)
        .collect(Collectors.toList());

    if (!synclabs.isEmpty()) {
      sb.append("synclabs: ");
      sb.append(String.join(", ", synclabs));
      sb.append(";\n\n");
    }

    for (State s : states) {
      // Write the state declaration
      sb.append(s.toImitator() + "\n");
    }
    sb.append("\n");

    // Add end machine
    sb.append("end (* " + name + " *)\n\n");

    return sb.toString();
  }
}

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class ParametricTimedAutomaton {
  private String name;
  private boolean isNetwork = false;
  private Set<String> initialConstraint = new HashSet<String>();
  private Set<String> syncLabels = new HashSet<String>();
  private ClockDeclaration clockDeclaration = new ClockDeclaration();
  private DiscreteDeclaration discreteDeclaration = new DiscreteDeclaration();
  private ParameterDeclaration parameterDeclaration = new ParameterDeclaration();
  private ConstDeclaration constDeclaration = new ConstDeclaration();
  private List<Automaton> automata = new ArrayList<Automaton>();

  /**
   * Constructor of the class
   *
   * @param name name of the model
   */
  public ParametricTimedAutomaton(String name) {
    this.name = name;
  }

  /**
   * Return the clocks of the model
   *
   * @return List of clock names
   */
  public List<String> getClocks() {
    return new ArrayList<>(clockDeclaration.clocks());
  }

  /**
   * Add a new clock to the model
   *
   * @param clk name of the clock
   */
  public void addClock(String clk) {
    this.clockDeclaration.clocks().add(clk);
  }

  /**
   * Add a synchronisation label to the model
   *
   * @param label new label
   */
  public void addSyncLabel(String label) {
    this.syncLabels.add(label);
  }

  /**
   * Add an initial constraint to the model
   *
   * @param constraint new constraint
   */
  public void addInitialConstraint(String constraint) {
    this.initialConstraint.add(constraint);
  }

  /**
   * Return the discrete variables of the model
   *
   * @return List of discrete variables
   */
  public List<String> getDiscretes() {
    return new ArrayList<>(discreteDeclaration.discretes());
  }

  /**
   * Add a new discrete variable to the model
   *
   * @param var new discrete variable
   */
  public void addDiscreteVariable(String var) {
    this.discreteDeclaration.discretes().add(var);
  }

  /**
   * Return the parameters of the model
   *
   * @return List of parameters
   */
  public List<String> getParameters() {
    return new ArrayList<>(parameterDeclaration.parameters());
  }

  /**
   * Add a new parameter to the model
   *
   * @param p new parameter
   */
  public void addParameter(String p) {
    this.parameterDeclaration.parameters().add(p);
  }

  /**
   * Return the constants of the model
   *
   * @return List of constants
   */
  public List<Constant> getConstants() {
    return new ArrayList<>(constDeclaration.constants());
  }

  /**
   * Add a new constant
   *
   * @param c new constant
   */
  public void addConstant(String name, String value) {
    this.constDeclaration.constants().add(new Constant(name, value));
  }

  /**
   * Add a new automaton to the model
   *
   * @param a new automaton
   */
  public void addAutomaton(Automaton a) {
    this.automata.add(a);

    /** by default add all the synclabels of the automaton if it's not a network */
    if (!isNetwork) {
      this.syncLabels.addAll(a.getSyncLabels());
    }
  }

  /**
   * Return the imitator code of a network of automata
   *
   * @return Imitator code
   */
  public String toImitator() {
    StringBuilder sb = new StringBuilder();

    // parse declarations
    sb.append("var\n\n");
    sb.append(clockDeclaration.toImitator());
    sb.append(constDeclaration.toImitator());
    sb.append(parameterDeclaration.toImitator());
    sb.append(discreteDeclaration.toImitator());

    // state name with initial state
    List<String> initialNodes = new ArrayList<String>();

    Set<String> syncSet = new HashSet<String>(syncLabels);
    for (Automaton a : automata) {
      // get imitator code of state
      sb.append(a.toImitator(syncSet));

      // For the initial state later
      for (State s : a.states()) {
        if (s.initialState()) {
          initialNodes.add("loc[" + a.name() + "] = " + s.name());
        }
      }
    }

    // add the automaton initial constraint
    sb.append("(************************************************************)\n");
    sb.append("(* Initial state *)\n");
    sb.append("(************************************************************)\n");
    sb.append("\n");

    // write the initial location
    sb.append("init :=\n");
    if (initialNodes.size() > 0) {
      sb.append("\t(*------------------------------------------------------------*)\n");
      sb.append("\t(* Initial location *)\n");
      sb.append("\t(*------------------------------------------------------------*)\n");
    }

    for (String initialNodeName : initialNodes) {
      sb.append("\t& " + initialNodeName + "\n\n");
    }

    // write the initial constraints
    if (initialConstraint.size() > 0) {
      sb.append("\t(*------------------------------------------------------------*)\n");
      sb.append("\t(* Initial constraints *)\n");
      sb.append("\t(*------------------------------------------------------------*)\n");
      for (String c : initialConstraint) {
        sb.append("\t& " + c + "\n");
      }
    }

    // close the automaton initial constraint
    sb.append(";\n\n");

    // add the automaton end
    sb.append("(************************************************************)\n");
    sb.append("(* The end *)\n");
    sb.append("(************************************************************)\n");

    // Add end file
    sb.append("end\n");

    return sb.toString();
  }
}