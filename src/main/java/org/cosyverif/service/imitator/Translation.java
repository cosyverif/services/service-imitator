package org.cosyverif.service.imitator;

import java.io.File;
import java.io.IOException;

import org.cosyverif.alligator.service.AnnotatedService.Task;
import org.cosyverif.alligator.service.BinaryService;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.annotation.Launch;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.alligator.service.annotation.Service;
import org.cosyverif.model.Model;

@Service(name = "Export into Imitator format", 
    help = "Export of this PTA in the Imitator format", 
    version = "1.0", 
    tool = "IMITATOR", 
    index = 3,
    authors = {
        "Jaime Arias",
    }, 
    keywords = {})
public class Translation extends BinaryService {
  GrmlParser parser;

  // Model file
  @Parameter(name = "Model", help = "PTA model", direction = Direction.IN, formalism = {
      "http://formalisms.cosyverif.org/parametric-timed-automaton.fml",
      "http://formalisms.cosyverif.org/network-pta.fml"
  })
  private Model model;

  // output
  @Parameter(name = "Imitator file", help = "File with the output", direction = Direction.OUT, contenttype = "plain/text")
  private File ImitatorFile = null;

  @Launch
  public Task executeBinary() throws IOException {
    // parse GrML model
    parser = GrmlParser.create(model);
    final File model = parser.parse();

    ImitatorFile = model.getAbsoluteFile();
    return null;
  }

}
