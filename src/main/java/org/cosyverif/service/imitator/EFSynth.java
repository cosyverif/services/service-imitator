package org.cosyverif.service.imitator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.exec.CommandLine;
import org.cosyverif.Configuration;
import org.cosyverif.alligator.service.AnnotatedService.Task;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.annotation.Example;
import org.cosyverif.alligator.service.annotation.Launch;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.alligator.service.annotation.Service;
import org.cosyverif.alligator.service.output.LineOutputHandler;

@Service(name = "EF Synthesis", help = "Compute the set of parameter valuations for which some location is reachable", version = "1.0", tool = "IMITATOR", index = 2, authors = {
    "Étienne André",
    "Jaime Arias",
}, keywords = {})
public class EFSynth extends Imitator {
  // State_predicate
  @Parameter(name = "State Predicate", help = "State predicates allow conjunction, disjunction and the use of global discrete variables", direction = Direction.IN)
  public String statePredicate = "";

  // Result
  @Parameter(name = "Output", help = "The result of Imitator", direction = Direction.OUT, contenttype = "plain/text")
  public File outputFile = null;

  // Constraint
  @Parameter(name = "Constraint", help = "Constraint guaranteeing reachability", direction = Direction.OUT, multiline = true)
  public String constraint = "";

  @Launch
  public Task launchEFSynth() throws IOException {
    parser = GrmlParser.create(model);
    final File model = parser.parse();

    /** Create property file */
    File outputDirectory = Configuration.instance().temporaryDirectory();
    File propertyFile = File.createTempFile("property", ".imiprop", outputDirectory);
    BufferedWriter writer = new BufferedWriter(new FileWriter(propertyFile));
    String property = "property := #synth EF (" + statePredicate + ");";
    writer.write(property);
    writer.close();

    // imitator command
    final CommandLine command = new CommandLine("./imitator/imitator");
    fillOptions(command);

    // add model
    command.addArgument(model.getAbsolutePath());

    // add property
    command.addArgument(propertyFile.getAbsolutePath());

    final Path outputFilePath = Paths.get(model.getParent(), model.getName() + ".res");
    return task(command).out(new LineOutputHandler(this) {
      boolean lineConstraint = false;
      String escapeLine;

      @Override
      public void call(String line) {
        escapeLine = line.replaceAll("\u001B\\[[;\\d]*m", "") + '\n';
        logs += escapeLine;

        // set flags
        if (escapeLine.contains("constraint guaranteeing")) {
          lineConstraint = true;
        }
        if (lineConstraint) {
          constraint += escapeLine;
          if (escapeLine.contains("This")) {
            lineConstraint = false;
          }
        }

        // return output file
        File f = new File(outputFilePath.toString());
        if (f.exists()) {
          outputFile = f;
        }
      }
    });
  }

  @Example(name = "Example of EF synthesis", help = "Example of EF synthesis")
  public EFSynth example() {
    EFSynth service = new EFSynth();
    service.model = loadModelResource("/models/coffee.grml");
    service.statePredicate = "loc[machine] = c_done";
    return service;
  }
}
