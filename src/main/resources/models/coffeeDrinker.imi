var

(* Clocks *)
   x, y1, y2
    : clock;

(* Constants *)
  MAX_SUGAR = 3
    : constant;

(* Parameters *)
  p_add_sugar, p_coffee, p_button, p_patience_max, p_work_max, p_work_min
    : parameter;

(* Discrete variables *)
  nb_sugar
    : discrete;


(************************************************************)
  automaton machine
(************************************************************)
synclabs: press, cup, coffee, sleep;

loc idle: invariant True
  when True sync press do {x := 0} goto add;

loc add: invariant x <= p_add_sugar
  when True sync press do {} goto add;
  when x = p_add_sugar sync cup do {x := 0} goto prepare;

loc prepare: invariant x <= p_coffee
  when x = p_coffee sync coffee do {x := 0} goto cdone;

loc cdone: invariant x <= 10
  when True sync press do {x := 0} goto add;
  when x = 10 sync sleep do {} goto idle;
end (* machine *)


(************************************************************)
  automaton researcher
(************************************************************)
synclabs: press, coffee;

loc research: invariant y1 <= p_work_max
  when y1 >= p_work_min sync press do {y1 := 0, nb_sugar := 0, y2 := 0} goto add;

loc add: invariant y2 <= p_patience_max & y1 <= p_button & nb_sugar <= MAX_SUGAR - 1
  when y1 = p_button & nb_sugar < MAX_SUGAR - 1 sync press do {y1 := 0, nb_sugar := nb_sugar + 1} goto add;
  when y1 = p_button & nb_sugar = MAX_SUGAR - 1 sync press do {nb_sugar := nb_sugar + 1} goto wait_;
  when y2 = p_patience_max do {} goto mad;
  when True sync coffee do {} goto mad;

loc wait_: invariant y2 <= p_patience_max
  when y2 < p_patience_max sync coffee do {y1 := 0, y2 := 0} goto research;
  when y2 = p_patience_max do {} goto mad;

loc mad: invariant True

end (* researcher *)


(************************************************************)
(* Initial state *)
(************************************************************)

init :=
	(*------------------------------------------------------------*)
	(* Initial location *)
	(*------------------------------------------------------------*)
  & loc[machine]		= idle
  & loc[researcher]	= research
;

(************************************************************)
(* The end *)
(************************************************************)
end
