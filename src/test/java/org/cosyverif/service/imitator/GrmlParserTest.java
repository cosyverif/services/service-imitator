/**
 *
 */
package org.cosyverif.service.imitator;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Scanner;

import org.cosyverif.Configuration;
import org.cosyverif.alligator.XML;
import org.cosyverif.alligator.service.util.FileUtility;
import org.cosyverif.alligator.util.Utility;
import org.cosyverif.model.Model;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author
 *
 */
public final class GrmlParserTest {
    private File directory;
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";

    /** Create temp folder */
    @Before
    public void setUp() {
        directory = Utility.createTemporaryDirectory();
        Configuration.instance()
                .temporaryDirectory(directory);
    }

    /** Delete temp folder */
    @After
    public void tearDown() {
        Utility.deleteDirectory(directory);
        Configuration.instance()
                .temporaryDirectory(new File(System.getProperty("java.io.tmpdir")));
    }

    /**
     * Prints a message with green color
     *
     * @param msg string to be printed
     */
    public void printSuccessMsg(String msg) {
        System.out.println(ANSI_GREEN + msg + ANSI_RESET);
    }

    /**
     * Loads a model from a GrML file in the classpath.
     *
     * @param path the path to the GrML file.
     * @return the model
     */
    public final Model loadModelResource(String path) {
        try {
            if (path.startsWith("/")) {
                path = path.substring(1);
            }

            File file = File.createTempFile("model_", ".grml", Configuration.instance().temporaryDirectory());
            FileUtility.copyFile(getClass().getClassLoader().getResource(path).openConnection().getURL(), file);
            Model result = (Model) XML.fromFile(file);
            file.delete();
            return result;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Loads a file in the classpath.
     *
     * @param path
     *             the path to the file.
     * @return the file
     */
    public final File loadFileResource(String path) {
        try {
            if (path.startsWith("/")) {
                path = path.substring(1);
            }
            File file = File.createTempFile("file_", ".tmp", Configuration.instance()
                    .temporaryDirectory());
            FileUtility.copyFile(getClass().getClassLoader()
                    .getResource(path)
                    .openConnection()
                    .getURL(), file);
            return file;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void compareEqual(File expected, File actual) throws IOException {
        // Read the contents of the output file into a string
        String outputString = Files.readString(actual.toPath()).replaceAll("\\s+", " ");
        String expectedOutput = Files.readString(expected.toPath()).replaceAll("\\s+", " ");

        assertEquals(expectedOutput, outputString);
    }

    /**
     * Read the contents of a file into a string.
     *
     * @param file The file to read from
     * @return The contents of the file as a string
     * @throws FileNotFoundException If the file does not exist
     */
    public String fileToString(File file) throws FileNotFoundException {
        // Read the contents of the output file into a string
        StringBuilder fileContent = new StringBuilder();

        // Create the scanner
        Scanner sc = new Scanner(file);

        while (sc.hasNextLine()) {
            fileContent.append(sc.nextLine() + "\n");
        }
        sc.close();

        return fileContent.toString();
    }

    @Test
    public void testConstructor() {
        // Create a model object to pass to the GrmlParser constructor
        Model model = new Model();
        // Create a GrmlParser object using the constructor
        GrmlParser parser = new GrmlParser(model);
        // Check that the model field of the GrmlParser object is set correctly
        assertEquals(model, parser.model());
    }

    @Test
    public void testEmptyParse() {
        try {
            // Create parser
            GrmlParser parser = new GrmlParser(loadModelResource("/models/emptyModel.grml"));

            // Call the parse method
            final File model = parser.parse();
            File expected = loadFileResource(("/models/empty.imi"));
            compareEqual(expected, model);
            printSuccessMsg("[SUCCESS] testEmptyParse");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCoffeeModelParse() {
        try {
            // Create parser
            GrmlParser parser = new GrmlParser(loadModelResource("/models/coffee.grml"));

            // Call the parse method
            final File model = parser.parse();
            File expected = loadFileResource(("/models/coffee.imi"));
            compareEqual(expected, model);
            printSuccessMsg("[SUCCESS] testCoffeeParse");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDrinkerModelParse() {
        try {
            // Create parser
            GrmlParser parser = new GrmlParser(loadModelResource("/models/drinker.grml"));

            // Call the parse method
            final File model = parser.parse();
            File expected = loadFileResource(("/models/drinker.imi"));
            compareEqual(expected, model);
            printSuccessMsg("[SUCCESS] testDrinkerParse");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCoffeeDrinkerModelParse() {
        try {
            // Create parser
            GrmlParser parser = new GrmlParser(loadModelResource("/models/coffeeDrinker.grml"));

            // Call the parse method
            final File model = parser.parse();
            File expected = loadFileResource(("/models/coffeeDrinker.imi"));
            compareEqual(expected, model);
            printSuccessMsg("[SUCCESS] testCoffeeDrinkerParse");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
